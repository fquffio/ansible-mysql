MySQL
=====

This role's duty is to install and set up a [MySQL](http://www.mysql.com/) server.

Variables
---------

### `mysql_root_password`
The password of the root user. By default, this is randomly generated.

### `mysql_databases`
A list of MySQL databases to be created. By default, no database will be created.

### `mysql_users`
A list of dictionaries of users to be created, where:

- a **required** key `username` is the username to be used to connect to the server
- a **required** key `password` is the password to be set on user creation
- a **required** key `perms` is the list of permissions to be granted to the user
- a **required** key `hosts` is a list of allowed hosts for incoming connections

By default, no user will be created.

Example
-------

```yaml
---
mysql_root_password: "ro0T"
mysql_databases:
  - bedita
  - bedita_default
  - bedita_test
mysql_users:
  - username: bedita
    password: "B3D!T4"
    perms:
      - bedita.*:ALL
      - bedita\_%.*:ALL  # Grant privilege to both bedita_default and bedita_test
    hosts:
      - localhost
      - 127.0.0.1
      - ::1
```
